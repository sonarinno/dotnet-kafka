﻿using System;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace producer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var topic = "admintome-test";
            //var partition = 0;

            var config = new ProducerConfig { BootstrapServers = "10.253.105.33:31836" };

            // If serializers are not specified, default serializers from
            // `Confluent.Kafka.Serializers` will be automatically used where
            // available. Note: by default strings are encoded as UTF8.
            using (var p = new ProducerBuilder<Null, string>(config).Build())
            {
                try
                {
                    var dr = await p.ProduceAsync(topic, new Message<Null, string> { Value = "test" });
                    Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
                }
                catch (ProduceException<Null, string> e)
                {
                    Console.WriteLine($"Delivery failed: {e.Error.Reason}");
                }
            }
        }
    }
}
